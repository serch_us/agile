CONTRIBUTORS
============

Go to the agile content repository, https://bitbucket.org/serch_us/agile_content/

**One-time, create a fork for your organization.**

* Under ACTIONS, click on Fork, click on Fork repository


**For each edit session.**

* Under ACTIONS, click on Create branch
* Select branch from master
* Fill in Branch name

**Create a new file.**

* Under NAVIGATION, go to the Source page
* From the drop-down, select your branch
* Click on the New File button
* Fill in the filename with your organization followed by '/', to create a a file in your organization folder
* Enter the filename
* In the syntax mode, select reStructuredText
* Edit the document, when complete press Commit

**Submit edits.**

* Under ACTIONS, click Create pull request


EDITOR
======

**To review:**

	git subtree pull --prefix=content --squash shared master
	
	fab build
	
	fab serve
	
Then check http://localhost:8000/feeds/rss.xml

**To publish:**

	git subtree pull --prefix=content --squash shared master

	fab build
	
	cd agile.serch.us
	
	s3cmd sync . s3://agile.serch.us